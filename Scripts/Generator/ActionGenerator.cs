using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionGenerator : MonoBehaviour
{
    public Sprite action_sprite;
    public GameObject prefab;
    public Transform parent;
    public Color32 movement_color;
    public Color32 spell_color;

    public void Generate(MapData mapData, TileDataObj tileDataObj, EntityStatsObj entityStatsObj){
        foreach (Transform child in parent){
            Destroy(child.gameObject);
        }

        int energy = entityStatsObj.GetEntityTemporyStats().GetEnergy();
        float maxDistance = Mathf.FloorToInt(energy / 2);

        for (int i = 1; i <= maxDistance; i++){
            //side
            AskCreateAction(mapData, new Vector3(i, 0), tileDataObj);
            AskCreateAction(mapData, new Vector3(-i, 0), tileDataObj);
            AskCreateAction(mapData, new Vector3(0, i), tileDataObj);
            AskCreateAction(mapData, new Vector3(0, -i), tileDataObj);

            //diag
            if (i % 2 == 0){
                AskCreateAction(mapData, new Vector3(i/2, i/2), tileDataObj);
                AskCreateAction(mapData, new Vector3(-i/2, -i/2), tileDataObj);
                AskCreateAction(mapData, new Vector3(-i/2, i/2), tileDataObj);
                AskCreateAction(mapData, new Vector3(i/2, -i/2), tileDataObj);
            }

            //other
            if (i > 3){
                AskCreateAction(mapData, new Vector3(i - 3, -i + 1), tileDataObj);
            }
        }
    }

    private void AskCreateAction(MapData mapData, Vector3 gap, TileDataObj tileDataObj){
        Vector2Int newCoords = tileDataObj.GetTileData().GetCoords() + new Vector2Int((int)gap.x, (int)gap.y);
        TileData newTileData = mapData.GetTileData(newCoords);

        //if (newTileData == null) return;
        //if (newTileData.GetValue() != 0) return;
        //if (newTileData.GetEntityStats() != null) return;

        CreateAction(gap, tileDataObj);    
    }
    private void CreateAction(Vector3 gap, TileDataObj tileDataObj){
        GameObject action = Instantiate(prefab);
        action.transform.position = tileDataObj.transform.position + new Vector3(gap.x, -gap.y);
        action.GetComponent<SpriteRenderer>().sprite = action_sprite;
        action.GetComponent<SpriteRenderer>().color = movement_color;
        action.GetComponent<SpriteRenderer>().sortingOrder = 1;
        action.transform.parent = parent;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityGenerator : MonoBehaviour
{
    public string tagName;
    public GameObject prefab;
    public Transform parent;
    public List<EntityStatsObj> entityStatsObjList;

    public void Generate(MapData mapData){
        for (int x = 0; x < mapData.GetSize(); x++){
            for (int y = 0; y < mapData.GetSize(); y++){
                TileData tileData = mapData.GetTileData(x, y);
                EntityStats entityStats = tileData.GetEntityStats();

                if (entityStats == null) continue;

                GameObject obj = Instantiate(prefab);
                obj.transform.position = new Vector2(tileData.GetCoordX() - 0.02f, -tileData.GetCoordY() - 0.1f);
                obj.name = entityStats.GetEntityData().name;
                obj.transform.parent = parent;

                obj.GetComponent<SpriteRenderer>().sprite = entityStats.GetEntityData().spriteFront;
                obj.GetComponent<SpriteRenderer>().sortingOrder = 2;

                /*
                Collider2D col = obj.AddComponent<BoxCollider2D>();
                col.isTrigger = true;
                col.tag = tagName;
                */

                EntityStatsObj entityStatsObj = obj.AddComponent<EntityStatsObj>();
                entityStatsObj.SetEntityStats(entityStats);
                entityStatsObj.SetCoords(new Vector2Int(x, y));
                entityStatsObj.SetEntityTemporyStats(new EntityTemporyStats(entityStats));

                entityStatsObjList.Add(entityStatsObj);
            }
        }
    }

    public EntityStatsObj GetEntityStatsObjFromEntityStats(EntityStats entityStats){
        if (entityStats == null) return null;

        foreach (EntityStatsObj entityStatsObj in entityStatsObjList){
            if (entityStats == entityStatsObj.GetEntityStats()){
                return entityStatsObj;
            }
        }
        return null;
    }
}

using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public string tagName;
    public Sprite floor_sprite;
    public Sprite wall_sprite;
    public Sprite void_sprite;
    public GameObject prefab;
    public Transform parent;

    public void Generate(MapData mapData){
        for (int x = 0; x < mapData.GetSize(); x++){
            for (int y = 0; y < mapData.GetSize(); y++){
                TileData tileData = mapData.GetTileData(x, y);

                Sprite sprite = null;
                if (tileData.GetValue() == 0){
                    sprite = floor_sprite;
                }
                else if (tileData.GetValue() == 1){
                    sprite = wall_sprite;
                }
                else if (tileData.GetValue() == -1){
                    sprite = void_sprite;
                }

                GameObject obj = Instantiate(prefab);
                obj.transform.position = new Vector2(tileData.GetCoordX(), -tileData.GetCoordY());
                obj.name = "(" + tileData.GetCoordX() + ", " + tileData.GetCoordY() + ")";
                obj.GetComponent<SpriteRenderer>().sprite = sprite;
                obj.transform.parent = parent;

                Collider2D col = obj.AddComponent<BoxCollider2D>();
                col.isTrigger = true;
                col.tag = tagName;

                TileDataObj tileDataObj = obj.AddComponent<TileDataObj>();
                tileDataObj.SetTileData(tileData);
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapData
{
    private TileData[,] tilesData;
    private int size;

    public MapData(int size){
        this.size = size;
        this.tilesData = new TileData[size, size];
        for (int x = 0; x < size; x++){
            for (int y = 0; y < size; y++){
                this.tilesData[x, y] = new TileData(new Vector2Int(x, y));
            }
        }
    }

    public TileData GetTileData(Vector2Int coords){
        if (coords.x < 0 || coords.y < 0) return null;
        if (coords.x >= size || coords.y >= size) return null;

        return this.tilesData[coords.x, coords.y];
    }
    public TileData GetTileData(int x, int y){ return GetTileData(new Vector2Int(x, y)); }
    public int GetSize(){ return this.size; }

    public string DisplayTilesData(){
        string res = "DisplayTilesData : ";
        for (int x = 0; x < size; x++){
            for (int y = 0; y < size; y++){
                TileData tile = tilesData[x, y];
                res += "\n(" + tile.GetCoordX() + ", " + tile.GetCoordY() + ") -> " + tile.GetValue();
            }
        }
        return res;
    }
}

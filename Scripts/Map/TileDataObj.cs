using UnityEngine;

public class TileDataObj : MonoBehaviour
{
    private TileData tileData;

    public TileData GetTileData(){ return this.tileData; }

    public void SetTileData(TileData tileData){
        this.tileData = tileData;
    }
}

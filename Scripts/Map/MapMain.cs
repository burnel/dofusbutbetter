using UnityEngine;

public class MapMain : MonoBehaviour
{
    public string roomName;
    public MapTheme theme;
    public GameObject tilePrefab;
    public Transform parentOfCreatedRoom;

    private void Start()
    {
        MapBuilder builder = new MapBuilder(theme, tilePrefab, parentOfCreatedRoom);
        builder.Build(roomName);
    }
}
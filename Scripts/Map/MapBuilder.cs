using System.Collections.Generic;
using UnityEngine;

public class MapBuilder
{
    private readonly MapTheme _theme;
    private readonly GameObject _tilePrefab;
    private readonly Transform _parent;
    private readonly Vector3 ORIGIN = Vector3.zero;
    private readonly float TILE_WIDTH = 1f;
    private readonly float TILE_HEIGHT = 1f;
    private readonly List<GameObject> _instantiatedTiles = new();

    public MapBuilder(MapTheme theme, GameObject tilePrefab, Transform parent) {
        _theme = theme;
        _tilePrefab = tilePrefab;
        _parent = parent;
    }

    public void Build(string roomName) {
        TileType[,] tiles = TextureMapReader.Read(roomName);
        Quaternion rotation = Quaternion.identity;
        int dimX = tiles.GetLength(0);
        int dimY = tiles.GetLength(1);
        Vector3 upperLeftPos = ORIGIN + Vector3.left * dimX / 2 * TILE_WIDTH + Vector3.up * dimY / 2 * TILE_HEIGHT;
        for (int x = 0; x < dimX; x++) {
            for (int y = 0; y < dimY; y++) {
                Vector3 position = upperLeftPos + TILE_WIDTH * x * Vector3.right + TILE_HEIGHT * y * Vector3.down;
                Sprite sprite = _theme.GetSpriteFromTileType(tiles[x, y]);
                GameObject instantiatedObject = MonoBehaviour.Instantiate<GameObject>(_tilePrefab, position, rotation, _parent);
                instantiatedObject.GetComponent<SpriteRenderer>().sprite = sprite;
                _instantiatedTiles.Add(instantiatedObject);
            }
        }
    }

    public void DestroyOldRoom() {
        foreach(GameObject obj in _instantiatedTiles) {
            MonoBehaviour.Destroy(obj);
        }
    }
}
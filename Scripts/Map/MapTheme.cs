using UnityEngine;

[CreateAssetMenu(fileName = "Theme_X", menuName = "ScriptableObjects/MapTheme", order = 1)]
public class MapTheme : ScriptableObject
{
    public Sprite hole;
    public Sprite ground;
    public Sprite wall;
    public Sprite GetSpriteFromTileType(TileType tileType) {
        switch(tileType) {
            case TileType.HOLE: return hole;
            case TileType.WALL: return wall;
            default: return ground;
        }
    }
}
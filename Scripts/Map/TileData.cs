using UnityEngine;

public class TileData
{
    private int value;
    private EntityStats entityStats;
    private Vector2Int coords;

    public TileData(){
        this.value = 0;
        this.entityStats = null;
        this.coords = Vector2Int.zero;
    }
    public TileData(Vector2Int coords){
        this.value = 0;
        this.entityStats = null;
        this.coords = coords;
    }
    public TileData(int value, EntityStats entityStats, Vector2Int coords){
        this.value = value;
        this.entityStats = entityStats;
        this.coords = coords;
    }

    public int GetValue(){ return this.value; }
    public EntityStats GetEntityStats(){ return this.entityStats; }
    public Vector2Int GetCoords(){ return this.coords; }
    public int GetCoordX(){ return this.coords.x; }
    public int GetCoordY(){ return this.coords.y; }

    public void RemoveEntityStats(){
        this.entityStats = null;
    }
    public void SetEntityStats(EntityStats entityStats){
        this.entityStats = entityStats;
    }
    public void SetValue(int value){
        this.value = value;
    }
}

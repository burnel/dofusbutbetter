public enum TileType
{
    HOLE,
    GROUND,
    WALL,
    ENTRY,
    EXIT
}
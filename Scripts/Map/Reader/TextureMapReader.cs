using UnityEngine;

public class TextureMapReader
{
    public static TileType[,] Read(string roomName) {
        string fullName = "Rooms/" + roomName;
        Texture2D texture = Resources.Load<Texture2D>(fullName);
        Color32[] colors = texture.GetPixels32();
        TileType[,] tilesType = new TileType[texture.width, texture.height];
        for (int x = 0; x < texture.width; x++) {
            for (int y = 0; y < texture.height; y++) {
                int i = x + y * texture.width;
                tilesType[x, y] = MapPalette.GetTileTypeFromColor(colors[i]);
            }
        }
        return tilesType;
    }
}
public class PaletteNotRespectedException : System.Exception
{
    public PaletteNotRespectedException() { }
    public PaletteNotRespectedException(string message) : base(message) { }
}
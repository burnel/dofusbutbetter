using UnityEngine;

[ScriptableObject]
public class MapTheme
{
    public Sprite hole;
    public Sprite ground;
    public Sprite wall;
}
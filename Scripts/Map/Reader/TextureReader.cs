using UnityEngine;

public class TextureReader
{
    public static TileType[,] Read(string roomName) {
        string fullName = "Rooms/" + roomName;
        Texture2D texture = Ressources.Load<Texture2D>(fullName);
    }
}
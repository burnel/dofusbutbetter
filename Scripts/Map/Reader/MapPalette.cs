using UnityEngine;

public class MapPalette
{
    public static Color32 HOLE = new(0, 0, 0, 255);
    public static Color32 GROUND = new(127, 127, 127, 255);
    public static Color32 WALL = new(255, 255, 255, 255);
    public static Color32 ENTRY = new(0, 255, 0, 255);
    public static Color32 EXIT = new(255, 0, 0, 255);

    public static TileType GetTileTypeFromColor(Color32 color) {
       if (color.Equals(HOLE)) return TileType.HOLE;
       if (color.Equals(GROUND)) return TileType.GROUND;
       if (color.Equals(WALL)) return TileType.WALL;
       if (color.Equals(ENTRY)) return TileType.ENTRY;
       if (color.Equals(EXIT)) return TileType.EXIT;
       throw new PaletteNotRespectedException(color.ToString());
    }
}
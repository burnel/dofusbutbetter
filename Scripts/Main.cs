using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{
    public MapData mapData;
    public int size;
    public MapGenerator mapGenerator;
    public EntityDataList entityDataList;
    public EntityGenerator entityGenerator;
    public ActionGenerator actionGenerator;

    public TileDataObj tileSelected;

    private void Start(){
        mapData = new MapData(size);

        mapData.GetTileData(0, 0).SetValue(1);
        mapData.GetTileData(1, 0).SetValue(1);
        mapData.GetTileData(2, 0).SetValue(1);
        mapData.GetTileData(3, 0).SetValue(1);
        mapData.GetTileData(4, 0).SetValue(1);

        mapData.GetTileData(1, 2).SetValue(1);
        mapData.GetTileData(2, 2).SetValue(1);
        mapData.GetTileData(3, 2).SetValue(1);

        mapData.GetTileData(1, 3).SetValue(-1);
        mapData.GetTileData(2, 3).SetValue(-1);
        mapData.GetTileData(3, 3).SetValue(-1);

        mapData.GetTileData(1, 1).SetEntityStats(new EntityStats(entityDataList.entityData_Knight, null));
        mapData.GetTileData(3, 1).SetEntityStats(new EntityStats(entityDataList.entityData_Spider, null));

        mapGenerator.Generate(mapData);
        entityGenerator.Generate(mapData);
    }

    private void Update(){
        if (Input.GetKeyDown(KeyCode.Mouse0)){
            TileDataObj newTileSelected = GetTileDataObjMouse();

            if (newTileSelected != null && newTileSelected.GetTileData().GetEntityStats() != null){
                EntityStatsObj entityStatsObj = entityGenerator.GetEntityStatsObjFromEntityStats(newTileSelected.GetTileData().GetEntityStats());
                actionGenerator.Generate(this.mapData, newTileSelected, entityStatsObj);
            }

            this.tileSelected = newTileSelected;
        }
    }

    private TileDataObj GetTileDataObjMouse(){
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);

        if (hit && hit.collider.tag == "Tile"){
            return hit.transform.GetComponent<TileDataObj>();
        }

        return null;
    }
}

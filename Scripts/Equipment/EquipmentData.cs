using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/EquipmentData", order = 1)]
public class EquipmentData : ScriptableObject
{
    public enum EquipmentType { Helmet, Chestplate, Boots, Special }
    public EquipmentType equipmentType;

    public int health;
    public int mana;
    public int energy;
    public int resistance;
    public int physicDamage;
    public int magicDamage;
    public int po;
    public int critic;

    public Sprite sprite;
}

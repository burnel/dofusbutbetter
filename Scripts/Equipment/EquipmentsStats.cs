using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentsStats
{
    private EquipmentData helmetData;
    private EquipmentData chestplateData;
    private EquipmentData bootsData;
    private EquipmentData special1Data;
    private EquipmentData special2Data;
    private EquipmentData special3Data;

    public EquipmentsStats(EquipmentData helmetData, EquipmentData chestplateData, EquipmentData bootsData, EquipmentData special1Data, EquipmentData special2Data, EquipmentData special3Data){
        this.helmetData = helmetData;
        this.chestplateData = chestplateData;
        this.bootsData = bootsData;
        this.special1Data = special1Data;
        this.special2Data = special2Data;
        this.special3Data = special3Data;
    }

    public void EquipHelmet(EquipmentData helmetData){
        if (helmetData.equipmentType != EquipmentData.EquipmentType.Helmet) return;

        this.helmetData = helmetData;
    }
    public void EquipChestplate(EquipmentData chestplateData){
        if (chestplateData.equipmentType != EquipmentData.EquipmentType.Chestplate) return;

        this.chestplateData = chestplateData;
    }
    public void EquipBoots(EquipmentData bootsData){
        if (bootsData.equipmentType != EquipmentData.EquipmentType.Boots) return;

        this.bootsData = bootsData;
    }

    public int CanEquipSpecial(EquipmentData specialData){
        if (specialData.equipmentType != EquipmentData.EquipmentType.Special) return -1;

        if (this.special1Data == null){
            return 1;
        }
        if (this.special2Data == null){
            return 2;
        }
        if (this.special3Data == null){
            return 3;
        }
        return 0;
    }
    public void EquipSpecial(EquipmentData specialData){
        int canEquipSpecial = CanEquipSpecial(specialData);

        if (canEquipSpecial == -1) return;

        if (canEquipSpecial >= 1){
            if (canEquipSpecial == 1){
                this.special1Data = specialData;
            }
            else if (canEquipSpecial == 1){
                this.special1Data = specialData;
            }
            else if (canEquipSpecial == 1){
                this.special1Data = specialData;
            }
            return;
        }

        //choice ui
        return;
    }

    public EquipmentData GetHelmet() { return helmetData; }
    public EquipmentData GetChestplate() { return chestplateData; }
    public EquipmentData GetBoots() { return bootsData; }
    public EquipmentData GetSpecial1() { return special1Data; }
    public EquipmentData GetSpecial2() { return special2Data; }
    public EquipmentData GetSpecial3() { return special3Data; }

    public int GetHealth(){
        int res = 0;

        if (helmetData != null){
            res += helmetData.health;
        }
        if (chestplateData != null){
            res += chestplateData.health;
        }
        if (bootsData != null){
            res += bootsData.health;
        }
        if (special1Data != null){
            res += special1Data.health;
        }
        if (special2Data != null){
            res += special2Data.health;
        }
        if (special3Data != null){
            res += special3Data.health;
        }

        return res;
    }
    public int GetMana(){
        int res = 0;

        if (helmetData != null){
            res += helmetData.mana;
        }
        if (chestplateData != null){
            res += chestplateData.mana;
        }
        if (bootsData != null){
            res += bootsData.mana;
        }
        if (special1Data != null){
            res += special1Data.mana;
        }
        if (special2Data != null){
            res += special2Data.mana;
        }
        if (special3Data != null){
            res += special3Data.mana;
        }

        return res;
    }
    public int GetEnergy(){
        int res = 0;

        if (helmetData != null){
            res += helmetData.energy;
        }
        if (chestplateData != null){
            res += chestplateData.energy;
        }
        if (bootsData != null){
            res += bootsData.energy;
        }
        if (special1Data != null){
            res += special1Data.energy;
        }
        if (special2Data != null){
            res += special2Data.energy;
        }
        if (special3Data != null){
            res += special3Data.energy;
        }

        return res;
    }
    public int GetResistance(){
        int res = 0;

        if (helmetData != null){
            res += helmetData.resistance;
        }
        if (chestplateData != null){
            res += chestplateData.resistance;
        }
        if (bootsData != null){
            res += bootsData.resistance;
        }
        if (special1Data != null){
            res += special1Data.resistance;
        }
        if (special2Data != null){
            res += special2Data.resistance;
        }
        if (special3Data != null){
            res += special3Data.resistance;
        }

        return res;
    }
    public int GetPhysicDamage(){
        int res = 0;

        if (helmetData != null){
            res += helmetData.physicDamage;
        }
        if (chestplateData != null){
            res += chestplateData.physicDamage;
        }
        if (bootsData != null){
            res += bootsData.physicDamage;
        }
        if (special1Data != null){
            res += special1Data.physicDamage;
        }
        if (special2Data != null){
            res += special2Data.physicDamage;
        }
        if (special3Data != null){
            res += special3Data.physicDamage;
        }

        return res;
    }
    public int GetMagicDamage(){
        int res = 0;

        if (helmetData != null){
            res += helmetData.magicDamage;
        }
        if (chestplateData != null){
            res += chestplateData.magicDamage;
        }
        if (bootsData != null){
            res += bootsData.magicDamage;
        }
        if (special1Data != null){
            res += special1Data.magicDamage;
        }
        if (special2Data != null){
            res += special2Data.magicDamage;
        }
        if (special3Data != null){
            res += special3Data.magicDamage;
        }

        return res;
    }
    public int GetPo(){
        int res = 0;

        if (helmetData != null){
            res += helmetData.po;
        }
        if (chestplateData != null){
            res += chestplateData.po;
        }
        if (bootsData != null){
            res += bootsData.po;
        }
        if (special1Data != null){
            res += special1Data.po;
        }
        if (special2Data != null){
            res += special2Data.po;
        }
        if (special3Data != null){
            res += special3Data.po;
        }

        return res;
    }
    public int GetCritic(){
        int res = 0;

        if (helmetData != null){
            res += helmetData.critic;
        }
        if (chestplateData != null){
            res += chestplateData.critic;
        }
        if (bootsData != null){
            res += bootsData.critic;
        }
        if (special1Data != null){
            res += special1Data.critic;
        }
        if (special2Data != null){
            res += special2Data.critic;
        }
        if (special3Data != null){
            res += special3Data.critic;
        }

        return res;
    }
}

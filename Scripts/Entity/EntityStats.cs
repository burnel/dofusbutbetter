using UnityEngine;

public class EntityStats
{
    private EquipmentsStats equipmentsStats;
    private EntityData entityData;

    public EntityStats(EntityData entityData, EquipmentsStats equipmentsStats){
        this.entityData = entityData;
        this.equipmentsStats = equipmentsStats;

        this.health = -1;
        this.energyMax = entityData.energy;
        CalculStats();
    }

    public EntityData GetEntityData(){ return this.entityData; }
    public EquipmentsStats GetEquipmentsData(){ return this.equipmentsStats; }
    public int GetHealthMax(){ return this.healthMax; }
    public int GetManaMax(){ return this.manaMax; }
    public int GetEnergyMax(){ return this.energyMax; }
    public int GetResistanceMax(){ return this.resistanceMax; }
    public int GetPhysicDamageMax(){ return this.physicDamageMax; }
    public int GetMagicDamageMax(){ return this.magicDamageMax; }
    public int GetPoMax(){ return this.poMax; }
    public int GetCriticMax(){ return this.criticMax; }
    public int GetHealth(){ return this.health; }

    private int healthMax;
    private int manaMax;
    private int energyMax;
    private int resistanceMax;
    private int physicDamageMax;
    private int magicDamageMax;
    private int poMax;
    private int criticMax;

    private int health;


    public void CalculStats(){
        if (this.entityData == null || this.equipmentsStats == null) return;

        this.healthMax = this.entityData.health + this.equipmentsStats.GetHealth();
        this.manaMax = this.entityData.mana + this.equipmentsStats.GetMana();
        this.energyMax = this.entityData.energy + this.equipmentsStats.GetEnergy();
        this.resistanceMax = this.entityData.resistance + this.equipmentsStats.GetResistance();
        this.physicDamageMax = this.entityData.physicDamage + this.equipmentsStats.GetPhysicDamage();
        this.magicDamageMax = this.equipmentsStats.GetMagicDamage();
        this.poMax = this.equipmentsStats.GetPo();
        this.criticMax = this.equipmentsStats.GetCritic();

        //HEALTH
        if (this.health > this.healthMax){
            this.health = this.healthMax;
        }
        //SECURITY
        if (this.health == -1){
            this.health = this.healthMax;
        }
    }

    public void ApplyHeal(int amount){
        this.health += amount;

        if (this.health <= this.healthMax) return;

        this.health = this.healthMax;
    }
    public void ApplyDamage(int amount){
        this.health -= amount;

        if (this.health > 0) return;

        this.health = 0;
    }
    public bool IsDead(){
        return this.health <= 0;
    }
}

using UnityEngine;

public class EntityTemporyStats
{
    private EntityStats entityStats;

    private int mana;
    private int energy;
    private int resistance;
    private int physicDamage;
    private int magicDamage;
    private int po;
    private int critic;
    private bool hasPlayed;

    public EntityTemporyStats(EntityStats entityStats){
        if (entityStats == null) return;

        this.entityStats = entityStats;
        this.hasPlayed = false;
        ResetStats();
    }

    public void ResetStats(){
        this.mana = entityStats.GetManaMax();
        this.energy = entityStats.GetEnergyMax();
        this.resistance = entityStats.GetResistanceMax();
        this.physicDamage = entityStats.GetPhysicDamageMax();
        this.magicDamage = entityStats.GetMagicDamageMax();
        this.po = entityStats.GetPoMax();
        this.critic = entityStats.GetCriticMax();
    }

    public int GetMana(){ return this.mana; }
    public int GetEnergy(){ return this.energy; }
    public int GetResistance(){ return this.resistance; }
    public int GetPhysicDamage(){ return this.physicDamage; }
    public int GetMagicDamage(){ return this.magicDamage; }
    public int GetPo(){ return this.po; }
    public int GetCritic(){ return this.critic; }
    public bool GetHasPlayed(){ return this.hasPlayed; }

    /*
    public void SetMana(int amount){
        this.mana = amount;
    }
    public void SetEnergy(int amount){
        this.energy = amount;
    }
    public void SetResistance(int amount){
        this.resistance = amount;
    }
    public void SetPhysicDamage(int amount){
        this.physicDamage = amount;
    }
    public void SetMagicDamage(int amount){
        this.magicDamage = amount;
    }
    public void SetPo(int amount){
        this.po = amount;
    }
    public void SetCritic(int amount){
        this.critic = amount;
    }
    public void SetHasPlayed(bool status){
        this.hasPlayed = status;
    }
    */
}

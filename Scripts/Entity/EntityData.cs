using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/EntityData", order = 1)]
public class EntityData : ScriptableObject
{
    public int health;
    public int mana;
    public int energy;
    public int resistance;
    public int physicDamage;

    public Sprite spriteFront;
    public Sprite spriteBack;
    public Sprite spriteSide;
}

using UnityEngine;

public class EntityStatsObj : MonoBehaviour
{
    private EntityStats entityStats;
    private EntityTemporyStats entityTemporyStats;
    private Vector2Int coords;

    public EntityStats GetEntityStats(){ return this.entityStats; }
    public EntityTemporyStats GetEntityTemporyStats(){ return this.entityTemporyStats; }
    public Vector2Int GetCoords(){ return this.coords; }

    public void SetEntityStats(EntityStats entityStats){
        this.entityStats = entityStats;
    }
    public void SetEntityTemporyStats(EntityTemporyStats entityTemporyStats){
        this.entityTemporyStats = entityTemporyStats;
    }
    public void SetCoords(Vector2Int coords){
        this.coords = coords;
    }
}
